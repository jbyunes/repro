#!/bin/sh

LG=EN-DE
echo "Preprocessing dataset Brute $LG"
VALID=input/valid
INPUT=input/$LG/Europarl-Brute
OUTPUT=output/datasets/$LG/Europarl-Brute
mkdir -p $OUTPUT
OUTPUT=$OUTPUT/preprocess
SRC=$INPUT/europarl-v7.de-en.en
TGT=$INPUT/europarl-v7.de-en.de
VSRC=$VALID/valid5000.en
VTGT=$VALID/valid5000.de
onmt_preprocess -train_src $SRC -train_tgt $TGT -valid_src $VSRC -valid_tgt $VTGT -src_vocab_size 35000 -tgt_vocab_size 35000 -src_seq_length 60 -tgt_seq_length 60 -save_data $OUTPUT 
