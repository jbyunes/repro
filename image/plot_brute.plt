set term png
set output 'output/tables_and_plots/brute_en_de.png'
set xlabel 'steps'
set ylabel 'BLEU score'
plot 'output/datasets/EN-DE/Europarl-Brute/mean_test.txt' with lines title "RAW en-de (test)", 'output/datasets/EN-DE/Europarl-Brute/mean_news.txt' with lines title "RAW en-de (news)"
