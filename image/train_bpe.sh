#!/bin/bash

#Brute
LG=EN-DE
INPUT=output/datasets/$LG/Europarl-BPE
OUTPUT=output/datasets/$LG/Europarl-BPE
onmt_train -src_word_vec_size 700 -tgt_word_vec_size 700 -train_steps 150000 -valid_steps 10000 -optim adadelta -save_model $OUTPUT/model -data $INPUT/preprocess -world_size 1 -gpu_ranks 0 > $OUTPUT/train.log 2>&1
