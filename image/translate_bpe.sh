#!/bin/bash

#
# translate_test Europarl_BPE EN-DE en de 15000 
function translate_test() {
	OUT_PREFIX=output/datasets/$2/$1
	TSET=test
	SRC=input/${TSET}/test5000.$3
	TGT=input/${TSET}/test5000.$4
	SNAP=$5
	BLEU=${OUT_PREFIX}/bleu_${TSET}_step_${SNAP}
	TRANS=${OUT_PREFIX}/translation_${TSET}_step_${SNAP}
	MODEL=${OUT_PREFIX}/model_step_${SNAP}.pt
	MEAN=${OUT_PREFIX}/mean_${TSET}

	echo "	Translating test5000 for $2 with snapshot at: $5"
	onmt_translate -gpu 0 -replace_unk -model $MODEL -src $SRC -tgt $TGT  -output $TRANS.txt > $TRANS.log 2>&1
	sacrebleu --sentence-level $TGT --input $TRANS.txt > $BLEU.txt
	cut -f3 -d' ' $BLEU.txt | awk "{sum += \$1} END {print "$SNAP",sum/NR}" >> $MEAN.txt

	OUT_PREFIX=output/datasets/$2/$1
	TSET=news
	SRC=input/$2/${TSET}/newstest2013.$3
	TGT=input/$2/${TSET}/newstest2013.$4
	SNAP=$5
	BLEU=${OUT_PREFIX}/bleu_${TSET}_step_${SNAP}
	TRANS=${OUT_PREFIX}/translation_${TSET}_step_${SNAP}
	MODEL=${OUT_PREFIX}/model_step_${SNAP}.pt
	MEAN=${OUT_PREFIX}/mean_${TSET}

	echo "	Translating news2013 for $2 with snapshot at: $5"
	onmt_translate -gpu 0 -replace_unk -model $MODEL -src $SRC -tgt $TGT  -output $TRANS.txt > $TRANS.log 2>&1
	sacrebleu --sentence-level $TGT --input $TRANS.txt > $BLEU.txt
	cut -f3 -d' ' $BLEU.txt | awk "{sum += \$1} END {print "$SNAP",sum/NR}" >> $MEAN.txt
}


DATA_SETS="Europarl-BPE"
LG=EN-DE
SUFFIXES="en de"
for DATA_SET in $DATA_SETS
do
	echo "Translating dataset: $DATA_SET for $LG"
	STEP=5000
	while [ $STEP -le 150000 ]
	do
		translate_test $DATA_SET $LG $SUFFIXES $STEP
		STEP=`expr $STEP + 5000`
	done
done
